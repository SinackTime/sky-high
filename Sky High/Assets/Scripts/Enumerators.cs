﻿public enum Direction { NORTH, SOUTH, EAST, WEST };
public enum WallType { DOOR, WALL, BARS, ONEWAY };
public enum RoomType { START, NORMAL, ITEM, BOSS };
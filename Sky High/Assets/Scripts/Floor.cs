﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Floor : MonoBehaviour
{
    public int maxX = 5, maxY = 5;                      //max X and y coordinates on a map
    Coordinates origin;

    List<Room> rooms;
    List<GameObject> roomPrefabs;

    public void Initilize(int maxHeight, int maxWidth, int numberOfRooms, List<GameObject> roomPrefabList)
    {
        maxX = maxWidth;
        maxY = maxHeight;
        roomPrefabs = roomPrefabList;
        rooms = new List<Room>();

        GenerateNewFloor(numberOfRooms);
        GenerateFloorPathsAndAttributes();
        GenerateFloorGameObjects();

        //rooms are currently created on a coordinate grid
        //rooms currently have been assigned properties
        //        the room gameObjects have not been generated yet
        // **************************** NEXT STEP: CONTINUE ROOM GENERATION *******************
    }

    //code that generates a new floor full of rooms
    void GenerateNewFloor(int numberOfRooms)
    {
        //set the origin room to the middle
        origin = new Coordinates((maxX / 2) + 1, (maxY / 2) + 1);

        //add a number of rooms equal to the passed in variable
        for (int idx = 0; idx < numberOfRooms; idx++)
        {
            rooms.Add(GenerateRoom());
        }

        return;
    }

    //links all generated rooms with paths
    void GenerateFloorPathsAndAttributes()
    {
        List<Room> specialRooms = new List<Room>();
        Room startRoom = rooms[Random.Range(0, rooms.Count)];
        Room endRoom = FindLongestPath(startRoom);
        Room itemRoom = rooms[Random.Range(0, rooms.Count)];

        //ensure item room is not (or next to) the starting room or ending room
        while (IsCoodinateAdjacent(itemRoom.coordinates, startRoom.coordinates)
            || IsCoodinateAdjacent(itemRoom.coordinates, endRoom.coordinates))
            itemRoom = rooms[Random.Range(0, rooms.Count)];

        if (endRoom == startRoom)
            Debug.Log("ERROR in finding a suitable boss room");

        startRoom.roomType = RoomType.START;
        itemRoom.roomType = RoomType.ITEM;
        endRoom.roomType = RoomType.BOSS;

        specialRooms.Add(startRoom);
        specialRooms.Add(itemRoom);
        specialRooms.Add(endRoom);

        //go through the room list and change the roomtype to normal unless it is a special room already
        foreach (Room room in rooms)
        {
            if (!specialRooms.Contains(room))
            {
                room.roomType = RoomType.NORMAL;
            }
        }

        //create the boss room and then a path to the entry room
        CreateSingleEntryRoom(endRoom, FaceLocation(endRoom.coordinates, startRoom.coordinates), WallType.BARS);
        CreatePath(endRoom, startRoom);

        //create item room and then a path to the entry room
        CreateAllEntryRoom(itemRoom, WallType.BARS);
        CreatePath(itemRoom, startRoom);

        //go through the room list and ensure there is a path going to the closest special room
        foreach (Room room in rooms)
        {
            if (room.roomType == RoomType.NORMAL)
            {
                CreatePath(room, FindShortestPath(room, specialRooms));
            }
        }

        //create all doors sourrounding the start room
        CreateAllEntryRoom(startRoom, WallType.DOOR);

        //go though the room list and ensure all rooms have all sides with a wall or door
        foreach (Room room in rooms)
        {
            if (room.roomType == RoomType.NORMAL)
            {
                CreateAllEntryRoom(room, RandomizeEntry(true));
            }
        }
    }

    //create room game objects according to each rooms' attributes
    void GenerateFloorGameObjects()
    {


    }

    //generate a new room in a random location, starting from the center of the map
    Room GenerateRoom()
    {
        Room newRoom = new Room();
        Coordinates newRoomCoordinates = new Coordinates(-1, -1);

        if (rooms != null && rooms.Count > 0)
        {
            while (newRoomCoordinates.x == -1)
            {
                //randomly generate a room index
                int index = Random.Range(0, rooms.Count);
                Direction first, second, third, fourth;
                first = second = third = fourth = 0;
                RandomizeDirections(ref first, ref second, ref third, ref fourth);

                //check to see if there is an un-occuped room on each edge and assign the coordinate if unoccupied
                if (!IsCoordinateOccupied(rooms[index].coordinates.GetAdjacentCoordinate(first)))
                    newRoomCoordinates = rooms[index].coordinates.GetAdjacentCoordinate(first);
                if (!IsCoordinateOccupied(rooms[index].coordinates.GetAdjacentCoordinate(second)))
                    newRoomCoordinates = rooms[index].coordinates.GetAdjacentCoordinate(second);
                if (!IsCoordinateOccupied(rooms[index].coordinates.GetAdjacentCoordinate(third)))
                    newRoomCoordinates = rooms[index].coordinates.GetAdjacentCoordinate(third);
                if (!IsCoordinateOccupied(rooms[index].coordinates.GetAdjacentCoordinate(fourth)))
                    newRoomCoordinates = rooms[index].coordinates.GetAdjacentCoordinate(fourth);
            }
        }
        else
        {
            newRoomCoordinates = origin;
        }
        newRoom.coordinates = newRoomCoordinates;

        newRoom.Initilize();

        return newRoom;
    }

    //create a room with three walls and a single point of entry
    void CreateSingleEntryRoom(Room currentRoom, Direction doorDirection, WallType doorType)
    {
        Direction first, second, third, fourth;
        first = second = third = fourth = 0;

        //create the current room with only one entrance facing the specified direction
        RandomizeDirections(ref first, ref second, ref third, ref fourth);
        BuildWallOfType(currentRoom, doorDirection, doorType);
        if (first != doorDirection)
            BuildWallOfType(currentRoom, first, WallType.WALL);
        if (second != doorDirection)
            BuildWallOfType(currentRoom, second, WallType.WALL);
        if (third != doorDirection)
            BuildWallOfType(currentRoom, third, WallType.WALL);
        if (fourth != doorDirection)
            BuildWallOfType(currentRoom, fourth, WallType.WALL);
    }

    //create a room with no walls and all points of entry
    void CreateAllEntryRoom(Room currentRoom, WallType doorType)
    {
        Direction first, second, third, fourth;
        first = second = third = fourth = 0;

        //create all single specified doortype around the current room generated in a random order
        RandomizeDirections(ref first, ref second, ref third, ref fourth);
        BuildWallOfType(currentRoom, first, doorType);
        BuildWallOfType(currentRoom, second, doorType);
        BuildWallOfType(currentRoom, third, doorType);
        BuildWallOfType(currentRoom, fourth, doorType);
    }

    //check to see if two coordinates are next to eachtoher
    bool IsCoodinateAdjacent(Coordinates firstCoordinate, Coordinates secondCoordinate)
    {
        if (firstCoordinate.ToVector2() == secondCoordinate.ToVector2())
            return true;
        if (firstCoordinate.GetAdjacentCoordinate(Direction.NORTH).ToVector2() == secondCoordinate.ToVector2())
            return true;
        if (firstCoordinate.GetAdjacentCoordinate(Direction.SOUTH).ToVector2() == secondCoordinate.ToVector2())
            return true;
        if (firstCoordinate.GetAdjacentCoordinate(Direction.EAST).ToVector2() == secondCoordinate.ToVector2())
            return true;
        if (firstCoordinate.GetAdjacentCoordinate(Direction.WEST).ToVector2() == secondCoordinate.ToVector2())
            return true;

        return false;
    }

    //randomizes what kind of door the player has to deal with based on level difficulty
    WallType RandomizeEntry()
    {
        Debug.Log("Adding a door entry, the doorways have not been randomized yet");
        return WallType.DOOR;
    }

    WallType RandomizeEntry(bool wallsIncluded)
    {
        Debug.Log("Adding a door entry, the doorways and walls have not been randomized yet");
        return WallType.DOOR;
    }

    //create a path from one room location to another
    void CreatePath(Room startingRoom, Room endingRoom)
    {
        //face the direction of the ending room from the perspetive of the starting room
        Direction facing = FaceLocation(startingRoom.coordinates, endingRoom.coordinates);

        //create a path until we reach the ending room
        Room currentRoom = GetRoom(startingRoom.coordinates.GetAdjacentCoordinate(facing));
        while (currentRoom != endingRoom)
        {
            //update direction facing the ending room from the perspective of the current room
            facing = FaceLocation(currentRoom.coordinates, endingRoom.coordinates);
            //build an entry way to ensure there is at least a single path to the ending room
            BuildWallOfType(currentRoom, facing, RandomizeEntry());
            //set the current room to be one room closer to the ending room
            currentRoom = GetRoom(currentRoom.coordinates.GetAdjacentCoordinate(facing));
        }
    }

    //builds a door if the wallspace is not occupied, builds a wall if there is nothing occupying the adjacent space
    void BuildWallOfType(Room room, Direction direction, WallType wallType)
    {
        //check to see if there is already a wall connection and return if that is the case
        if (IsThereAWallConnectorAlready(rooms[GetRoomIndex(room)].coordinates, direction))
        {
            return;
        }
        //check to see if there are occuped rooms on each edge and assign them connectors provided there are not connectiors already assigned
        else if (IsCoordinateOccupied(rooms[GetRoomIndex(room)].coordinates.GetAdjacentCoordinate(direction))
            && !IsThereAWallConnectorAlready(rooms[GetRoomIndex(room)].coordinates.GetAdjacentCoordinate(direction), Coordinates.GetOppositeDirection(direction)))
        {
            room.GenerateWall(direction, wallType);
            GetRoom(room.coordinates.GetAdjacentCoordinate(direction)).GenerateWall(Coordinates.GetOppositeDirection(direction), wallType);
        }
        //if there is a preexisting wall connector, mirror the same one to prevent inconsistencies
        else if (IsCoordinateOccupied(rooms[GetRoomIndex(room)].coordinates.GetAdjacentCoordinate(direction))
                && IsThereAWallConnectorAlready(rooms[GetRoomIndex(room)].coordinates.GetAdjacentCoordinate(direction), Coordinates.GetOppositeDirection(direction)))
        {
            room.GenerateWall(direction, GetRoom(room.coordinates.GetAdjacentCoordinate(direction)).WallConnector[Coordinates.GetOppositeDirection(direction)]);
        }
        //if there is no room next to this one, just generate a wall
        else
            room.GenerateWall(direction, WallType.WALL);
    }

    //shows which direction you would face when trying to generate a path
    Direction FaceLocation(Coordinates startCoordinates, Coordinates endCoordinates)
    {
        Direction directionX = Direction.EAST;
        Direction directionY = Direction.NORTH;
        int differenceX = -1, differenceY = -1;

        if (startCoordinates.x > endCoordinates.x)
        {
            directionX = Direction.WEST;
            differenceX = startCoordinates.x - endCoordinates.x;
        }
        else if (startCoordinates.x < endCoordinates.x)
        {
            directionX = Direction.EAST;
            differenceX = endCoordinates.x - startCoordinates.x;
        }

        if (startCoordinates.y > endCoordinates.y)
        {
            directionY = Direction.SOUTH;
            differenceY = startCoordinates.y - endCoordinates.y;
        }
        else if (startCoordinates.y < endCoordinates.y)
        {
            directionY = Direction.NORTH;
            differenceY = endCoordinates.y - startCoordinates.y;
        }

        if (differenceX > differenceY)
        {
            return directionX;
        }
        else if (differenceY > differenceX)
        {
            return directionY;
        }
        else if ((differenceX == differenceY) && differenceX != -1)
        {
            if (Random.Range(0, 1) == 0)
                return directionX;
            else
                return directionY;
        }


        Debug.Log("Cannot face any specific direction, defaulting to north");
        return Direction.NORTH;
    }

    //check to see if there is a wall connector on a specific corner of a room
    bool IsThereAWallConnectorAlready(Coordinates coordinate, Direction direction)
    {
        return GetRoom(coordinate).WallConnector.ContainsKey(direction);
    }

    //finds the longest path by direct distance in order to determine the room with the most distance
    Room FindLongestPath(Room startRoom)
    {
        float greatestDistance = 0f;
        Room endRoom = startRoom;

        foreach (Room room in rooms)
        {
            if (Vector2.Distance(startRoom.coordinates.ToVector2(), room.coordinates.ToVector2()) > greatestDistance)
            {
                greatestDistance = Vector2.Distance(startRoom.coordinates.ToVector2(), room.coordinates.ToVector2());
                endRoom = room;
            }
        }

        return endRoom;
    }

    //finds the shortest path by direct distance in order to determine the room with the least distance
    Room FindShortestPath(Room startRoom)
    {
        float leastDistance = 0f;
        Room endRoom = startRoom;

        foreach (Room room in rooms)
        {
            //ensure the starting room does not get considered
            if (room != startRoom)
            {
                if (Vector2.Distance(startRoom.coordinates.ToVector2(), room.coordinates.ToVector2()) < leastDistance)
                {
                    leastDistance = Vector2.Distance(startRoom.coordinates.ToVector2(), room.coordinates.ToVector2());
                    endRoom = room;
                }
            }
        }

        return endRoom;
    }

    //finds the shortest path by direct distance in order to determine the room with the least distance
    Room FindShortestPath(Room startRoom, List<Room> roomOptions)
    {
        float leastDistance = 0f;
        Room endRoom = startRoom;

        foreach (Room room in rooms)
        {
            //ensure the starting room or special room does not get considered
            if (room != startRoom || !roomOptions.Contains(room))
            {
                if (Vector2.Distance(startRoom.coordinates.ToVector2(), room.coordinates.ToVector2()) < leastDistance)
                {
                    leastDistance = Vector2.Distance(startRoom.coordinates.ToVector2(), room.coordinates.ToVector2());
                    endRoom = room;
                }
            }
        }

        return endRoom;
    }

    //check to see if a coodinate if occupied or if it is out of bounds
    bool IsCoordinateOccupied(Coordinates coordinate)
    {
        //ensure the coordinate is not out of bounds
        if (coordinate.x > maxX || coordinate.x < 1)
            return true;

        if (coordinate.y > maxY || coordinate.y < 1)
            return true;

        //check each room to see if the coordinate matches another
        foreach (Room room in rooms)
        {
            if (coordinate.ToVector2() == room.coordinates.ToVector2())
                return true;
        }

        return false;
    }

    //randomly generate four non consecutive directions
    void RandomizeDirections(ref Direction first, ref Direction second, ref Direction third, ref Direction fourth)
    {
        first = (Direction)Random.Range(0, 3);
        second = first;
        third = first;
        fourth = 0;

        while (second == first)
            second = (Direction)Random.Range(0, 3);
        while (third == first || third == second)
            third = (Direction)Random.Range(0, 3);
        while (fourth == first || fourth == second || fourth == third)
            fourth++;
    }

    //return a room based on the input coodinates
    Room GetRoom(Coordinates inputCoordinates)
    {
        //check each room to see if the coordinate matches another
        foreach (Room room in rooms)
        {
            if (room.coordinates.ToVector2() == inputCoordinates.ToVector2())
                return room;
        }

        Debug.Log("ERROR: Unable to find a room with coordinates:" + inputCoordinates.ToString());
        return null;
    }

    //finds the room index of the room being checked
    int GetRoomIndex(Room roomBeingChecked)
    {
        for (int index = 0; index < rooms.Count; index++)
        {
            if (rooms[index] == roomBeingChecked)
                return index;
        }

        Debug.Log("ERROR: unable to get room index of ", roomBeingChecked);
        return -1;
    }

}

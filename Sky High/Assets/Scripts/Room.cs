﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room : MonoBehaviour
{
    public Coordinates coordinates;
    public Dictionary<Direction, WallType> WallConnector;
    public RoomType roomType;

    public void Initilize()
    {
        WallConnector = new Dictionary<Direction, WallType>();
    }

    public void GenerateWall(Direction direction, WallType wallType)
    {
        if (WallConnector.ContainsKey(direction))
        {
            Debug.Log("ERROR: Wall Connector already exists on" + direction.ToString() + " cannot duplicate" + WallConnector.ToString());
            return;
        }

        //add the wall connector to the room
        WallConnector.Add(direction, wallType);
    }

    //script that generates items for a room
    void GenerateItems()
    {
        Debug.Log("This method does not do anything yet");


        return;
    }

    //script that generates NPCs for the room
    void GenerateNPCs()
    {
        Debug.Log("This method does not do anything yet");


        return;
    }

}

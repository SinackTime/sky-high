﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public struct Coordinates
{
    //x and y coordinate variables
    public int x, y;

    //coodinate contructor. Must take X and Y values when constructed
    public Coordinates(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    //making your coordinates a string will return this format
    public override string ToString()
    {
        return string.Format("({0},{1})", x, y);
    }

    //change your coordinate value to a vector2
    public Vector2 ToVector2()
    {
        return new Vector2(x, y);
    }

    public Coordinates GetAdjacentCoordinate(Direction direction)
    {
        int x = this.x;
        int y = this.y;
        switch (direction)
        {
            case Direction.NORTH:
                y += 1;
                break;
            case Direction.EAST:
                x += 1;
                break;
            case Direction.SOUTH:
                y -= 1;
                break;
            case Direction.WEST:
                x -= 1;
                break;
            default:
                break;
        }

        return new Coordinates(x, y);
    }

    public static Direction GetOppositeDirection(Direction direction)
    {
        switch (direction)
        {
            case Direction.NORTH:
                return Direction.SOUTH;
            case Direction.EAST:
                return Direction.WEST;
            case Direction.SOUTH:
                return Direction.NORTH;
            case Direction.WEST:
                return Direction.EAST;
            default:
                Debug.Log("Error, No direction has been set. Cannot find opposite direction");
                return 0;
        }
    }

}
